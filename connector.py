import requests
import pprint
import json

class Connector(object):

    @staticmethod
    def _create_base_url(ip: str, auth_token: str):
        return f"http://{ip}/api/v1/{auth_token}"

    @staticmethod
    def get_info(ip: str, auth_token: str):
        response = requests.get(f"{Connector._create_base_url(ip, auth_token)}/")
        if response.status_code < 300:
            return response.json()

    @staticmethod
    def put_animation(ip: str, auth_token: str, animation: str):
        body = {
            "write": {
                "command": "display",
                "animType": "custom",
                "animData": animation,
                "loop": True
            }
        }
        response = requests.put(f"{Connector._create_base_url(ip, auth_token)}/effects", data=json.dumps(body))

        if response.status_code != 204:
            print(response.reason)