from Color import Color


class Frame(object):

    def __init__(self, color: Color, duration: int):
        self._color = color
        self._duration = max(duration, 0)

    def __str__(self):
        return f"{str(self._color)} 0 {self._duration}"
