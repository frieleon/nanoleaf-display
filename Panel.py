from Frame import Frame
from Color import Color
from random import randint

from Samples import BUILDING_FRAMES_BLUE

class Panel(object):

    def __init__(self, panel_id: int, x: int, y: int, o: int):
        self._id = panel_id
        self._x = x
        self._y = y
        self._o = o
        self._frames = BUILDING_FRAMES_BLUE
        
    def __str__(self):
        frames = ""
        for frame in self._frames:
            frames += f"{str(frame)} "

        return f"{self._id} {len(self._frames)} " + frames
