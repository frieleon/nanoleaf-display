from Display import Display
import os

ADDRESS="192.168.178.22:16021"


def main():
    display = Display(ADDRESS, os.getenv("auth_token"))
    print(str(display))
    display.update()

if __name__ == '__main__':
    main()
