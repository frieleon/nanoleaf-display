from Color import Color
from Frame import Frame

COLOR_RED = Color(255, 0, 0)
COLOR_GREEN = Color(0, 255, 0)
COLOR_BLUE = Color(0, 0, 255)


FAILED_FRAME = [
    Frame(COLOR_RED, 1)
]

SUCCESSFUL_FRAME_BLUE = [
    Frame(COLOR_BLUE, 1)
]

SUCCESSFUL_FRAME_GREEN = [
    Frame(COLOR_GREEN, 1)
]

BUILDING_FRAMES_BLUE = [
    Frame(COLOR_BLUE, 10),
    Frame(Color(0, 128, 255), 10)
]

BUILDING_FRAMES_GREEN = [
    Frame(COLOR_GREEN, 10),
    Frame(Color(128, 255, 0), 10)
]

