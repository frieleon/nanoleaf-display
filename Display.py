from connector import Connector
from Panel import Panel
import pprint


class Display(object):

    def __init__(self, ip: str, auth_token: str):
        self._auth_token = auth_token
        self._ip = ip
        self._panels = []
        response = Connector.get_info(ip, auth_token)

        panels = response["panelLayout"]["layout"]["positionData"]

        for panel in panels:
            self._panels.append(Panel(panel["panelId"], panel["x"], panel["y"], panel["o"]))

    def __str__(self):
        panels = ""
        for panel in self._panels:
            panels += f"{str(panel)} "

        return f"{len(self._panels)} {panels}"

    def update(self):
        Connector.put_animation(self._ip, self._auth_token, str(self))