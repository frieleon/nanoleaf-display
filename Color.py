class Color(object):

    def __init__(self, red: int = 255, green: int = 255, blue: int = 255):
        """
        Color wrapper
        :param red: 0-255
        :param green: 0-255
        :param blue: 0-255
        """
        self._red = max(min(red, 255), 0)
        self._green = max(min(green, 255), 0)
        self._blue = max(min(blue, 255), 0)

    def __str__(self):
        return f"{self._red} {self._green} {self._blue}"

    @property
    def green(self):
        return self._green

    @property
    def red(self):
        return self._red

    @property
    def blue(self):
        return self._blue

    def set_green(self, green: int):
        if green in range(0, 255):
            self._green = green

    def set_red(self, red: int):
        if red in range(0, 255):
            self._red = red

    def set_blue(self, blue: int):
        if blue in range(0, 255):
            self._blue = blue
